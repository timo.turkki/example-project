from aws_cdk import (
  core,
  aws_ec2 as ec2,
  aws_ecs as ecs,
  aws_iam as iam,
  aws_ecr as ecr,
  aws_elasticloadbalancingv2 as elbv2,
  aws_logs as logs,
)

import os

class IacStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # VPC and Cluster Configuration

        vpc = ec2.Vpc.from_lookup(self, id="VPC", vpc_id="vpc-02b318197ae37afd5")

        cluster = ecs.Cluster(self, "apprentice-0", vpc=vpc)
        cluster.add_default_cloud_map_namespace(name="apprentice-0")

        # ECS Policies and Roles

        service_principal = iam.ServicePrincipal('ecs-tasks.amazonaws.com')
        task_role = iam.Role(self, 'ecsTaskExecutionRole-apprentice-0', assumed_by=service_principal)
        ecs_managed_policy = iam.ManagedPolicy.from_aws_managed_policy_name(
            'service-role/AmazonECSTaskExecutionRolePolicy'
        )
        task_role.add_managed_policy(ecs_managed_policy)

        # ECR Repositories

        frontend_ecr_repo = ecr.Repository.from_repository_name(
            self,
            id="frontend-apprentice-0",
            repository_name="apprentice-0-frontend"
        )

        backend_ecr_repo = ecr.Repository.from_repository_name(
            self,
            id="backend-apprentice-0",
            repository_name="apprentice-0-backend",
        )

        # Security groups

        frontend_security_group = ec2.SecurityGroup(
            self,
            'frontend-sg-apprentice-0',
            allow_all_outbound=True,
            security_group_name="FrontendSG-apprentice-0",
            vpc=vpc
        )

        backend_security_group = ec2.SecurityGroup(
            self,
            'backend-sg-apprentice-0',
            allow_all_outbound=True,
            security_group_name="BackendSG-apprentice-0",
            vpc=vpc
        )

        database_security_group = ec2.SecurityGroup(
            self,
            'database-sg-apprentice-0',
            allow_all_outbound=True,
            security_group_name="DatabaseSG-apprentice-0",
            vpc=vpc
        )

        frontend_security_group.connections.allow_from_any_ipv4(
            ec2.Port.tcp(5000),
        )
        backend_security_group.connections.allow_from_any_ipv4(
            ec2.Port.tcp(8080),
        )
        database_security_group.connections.allow_from_any_ipv4(
            ec2.Port.tcp(6379)
        )

        # Fargate Task Definitions

        frontend_task_definition = ecs.FargateTaskDefinition(
            self,
            'frontendTaskDefinition-apprentice-0',
            cpu=256,
            memory_limit_mib=512,
            task_role=task_role,
        )

        backend_task_definition = ecs.FargateTaskDefinition(
            self,
            'backendTaskDefinition-apprentice-0',
            cpu=256,
            memory_limit_mib=512,
            task_role=task_role
        )

        database_task_definition = ecs.FargateTaskDefinition(
            self,
            'databaseTaskDefinition-apprentice-0',
            cpu=256,
            memory_limit_mib=1024,
            task_role=task_role
        )

        # Logging

        frontend_log_group = logs.LogGroup(
            self,
            "frontendLogGroup-apprentice-0",
            log_group_name="/ecs/frontendLogGroup-apprentice-0",
            removal_policy=core.RemovalPolicy.DESTROY
        )

        backend_log_group = logs.LogGroup(
            self,
            "backendLogGroup-apprentice-0",
            log_group_name="/ecs/backendLogGroup-apprentice-0",
            removal_policy=core.RemovalPolicy.DESTROY
        )

        database_log_group = logs.LogGroup(
            self,
            "databaseLogGroup-apprentice-0",
            log_group_name="/ecs/databaseLogGroup-apprentice-0",
            removal_policy=core.RemovalPolicy.DESTROY
        )

        frontend_log_driver = ecs.AwsLogDriver(
            log_group=frontend_log_group,
            stream_prefix="frontend"
        )

        backend_log_driver = ecs.AwsLogDriver(
            log_group=backend_log_group,
            stream_prefix="backend",
        )
        
        database_log_driver = ecs.AwsLogDriver(
            log_group=database_log_group,
            stream_prefix="database",
        )

        # Container definitions

        frontend_container = frontend_task_definition.add_container(
            'frontend-apprentice-0',
            image=ecs.ContainerImage.from_ecr_repository(frontend_ecr_repo, tag="111"),
            logging=frontend_log_driver,
            environment={
                "COLORS_BACKEND_ADDRESS": "backend.apprentice-0:8080",
            },
            memory_limit_mib=512,
        )
        frontend_container.add_port_mappings(ecs.PortMapping(container_port=5000))

        backend_container = backend_task_definition.add_container(
            'backend-apprentice-0',
            image=ecs.ContainerImage.from_ecr_repository(backend_ecr_repo, tag="112"),
            logging=backend_log_driver,
            environment={
                "COLORS_DATABASE_ADDRESS": "database.apprentice-0:6379",
            },
            memory_limit_mib=512,
        )
        backend_container.add_port_mappings(ecs.PortMapping(container_port=8080))

        database_container = database_task_definition.add_container(
            'database-apprentice-0',
            image=ecs.ContainerImage.from_registry("redis:6.0-alpine"),
            logging=database_log_driver,
        )
        database_container.add_port_mappings(ecs.PortMapping(container_port=6379))

        # ECS Fargate Definitions

        frontend_service = ecs.FargateService(
            self,
            "FrontendFargateService-apprentice-0",
            cluster=cluster,            # Required
            task_definition=frontend_task_definition,
            security_group=frontend_security_group,
            desired_count=1,
            assign_public_ip=True,
            cloud_map_options=ecs.CloudMapOptions(name="frontend"),
            service_name="frontend"
        )

        ecs.FargateService(
            self,
            "BackendFargateService-apprentice-0",
            cluster=cluster,            # Required
            task_definition=backend_task_definition,
            security_group=backend_security_group,
            assign_public_ip=True,
            desired_count=1,
            cloud_map_options=ecs.CloudMapOptions(name="backend"),
            service_name="backend"
        )

        ecs.FargateService(
            self,
            "DatabaseFargateService-apprentice-0",
            cluster=cluster,            # Required
            task_definition=database_task_definition,
            security_group=database_security_group,
            desired_count=1,
            cloud_map_options=ecs.CloudMapOptions(name="database"),
            service_name="database"
        )

        # Application Load Balancer

        alb = elbv2.ApplicationLoadBalancer(
            self,
            "external-apprentice-0",
            vpc=vpc,
            internet_facing=True
        )

        alb_listener = alb.add_listener(
            'alb-listener-apprentice-0',
            port=80
        )

        alb_health_check = elbv2.HealthCheck(path='/')

        alb_listener.add_targets(
            'tg-apprentice-0',
            port=80,
            health_check=alb_health_check,
            targets=[frontend_service]
        )

        core.CfnOutput(self, 'ALB DNS: ', value=alb.load_balancer_dns_name)

