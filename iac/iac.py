from aws_cdk import (
  core,
  aws_ec2 as ec2,
  aws_ecs as ecs,
  aws_iam as iam,
  aws_ecr as ecr,
  aws_elasticloadbalancingv2 as elbv2,
  aws_logs as logs,
)
